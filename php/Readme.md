# Ducret-Bryan > Docker > **Php**

## 🖥️ How to use
- Edit the `Dockerfile` to change the build.
- Edit the `config.ini` use has a copy when container is build.

## 🖊️ How to build and store
- Install Docker
  - Login to the registry with your login and password or auth token (with a `read_registry` and `write_registry` at least)
  ```bash
  docker login registry.gitlab.com
  ```
- Prepare a new `VERSION_TAG` to used

### 🏗️ How to build
```bash
docker build -t registry.gitlab.com/ducret-bryan/docker/php:latest -t registry.gitlab.com/ducret-bryan/docker/php:{VERSION_TAG} .
```

### 💽 How to store
```bash
docker image push registry.gitlab.com/ducret-bryan/docker/php:latest && docker image push registry.gitlab.com/ducret-bryan/docker/php:{VERSION_TAG}
```
